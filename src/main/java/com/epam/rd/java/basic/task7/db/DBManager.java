package com.epam.rd.java.basic.task7.db;

import com.epam.rd.java.basic.task7.db.entity.Team;
import com.epam.rd.java.basic.task7.db.entity.User;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

public class DBManager {

	private static DBManager instance;
	private Connection connector;
	private static String URL = null;
	private static final String KEY = "connection.url";
	private static final String PATH = "./app.properties";

	public static synchronized DBManager getInstance() {
		//return null;
		if (instance == null){
			instance = new DBManager();
		}
		return instance;
	}

	private void connect() throws DBException{
		Connection connector;
		try {
			connector = DriverManager.getConnection(URL);
		} catch (SQLException e){
			e.printStackTrace();
			throw new DBException(e.getMessage(), e.getCause());
		}
		this.connector = connector;
	}

	private DBManager() {
		Properties props = new Properties();//null;
//		InputStream is = ClassLoader.getSystemResourceAsStream(PATH);
		//props = new Properties();
		try {
//			props.load(is);
//			if (is != null) {
//				is.close();
//			}
			props.load(new FileInputStream(PATH));
			if (props.containsKey(KEY)){
				URL = props.getProperty(KEY);
			}
			try {
				connect();
			} catch (DBException e){
				connector = null;
			}
		} catch (IOException e){
			connector = null;
		}
	}

	public boolean insertUser(User user) throws DBException {
		//return false;
//		String sql = "SELECT MAX(id) FROM users";
//		Connection connector;
//		try {
//			connector = DriverManager.getConnection(URL);
//		} catch (SQLException e){
//			throw new DBException(e.getMessage(), e.getCause());
//		}
//		Integer id;
//		Statement stmt;
//		try {
//			stmt = connector.createStatement();
//			ResultSet result = stmt.executeQuery(sql);
//			id = result.getInt("id");
//		} catch (SQLException e){
//			throw new DBException(e.getMessage(), e.getCause());
//		}
		boolean bool = false;
		String sql = "INSERT INTO users VALUES (DEFAULT, ?)";//"INSERT INTO users VALUES (?, ?)";
		String get = "SELECT id FROM users WHERE login = ?";
		if (connector == null) {
			connect();
		}
		PreparedStatement prepStmt;
		PreparedStatement getStmt;
		try {
			prepStmt = connector.prepareStatement(sql);
			//prepStmt.setInt(1, user.getId());
			//prepStmt.setString(2, user.getLogin());
			prepStmt.setString(1, user.getLogin());
			prepStmt.executeUpdate();
			bool = true;
			prepStmt.close();
		} catch (SQLException e){
			//e.printStackTrace();
			//throw new DBException(e.getMessage(), e.getCause());
			if (connector == null){
				e.printStackTrace();
				throw new DBException(e.getMessage(), e.getCause());
			}
		} finally {
			try {
				if (connector != null) {
					getStmt = connector.prepareStatement(get);
					getStmt.setString(1, user.getLogin());
					ResultSet result = getStmt.executeQuery();
					if (!result.next()) {
						throw new SQLException();
					}
					user.setId(result.getInt("id"));
					result.close();
					getStmt.close();
				} else {
					throw new SQLException();
				}
			} catch (SQLException e){
				e.printStackTrace();
				throw new DBException(e.getMessage(), e.getCause());
			}
			//throw new DBException(e.getMessage(), e.getCause());
		}
//		if (prepStmt != null) {
//			try {
//				prepStmt.close();
//			} catch (SQLException e) {
//				e.printStackTrace();
//				throw new DBException(e.getMessage(), e.getCause());
//			}
//		}
		return bool;
	}

	public boolean insertTeam(Team team) throws DBException {
		//return false;
		boolean bool = false;
		String sql = "INSERT INTO teams VALUES (DEFAULT, ?)";
		String get = "SELECT id FROM teams WHERE name = ?";
		if (connector == null) {
			connect();
		}
		PreparedStatement prepStmt;
		PreparedStatement getStmt;
		try {
			prepStmt = connector.prepareStatement(sql);
			//prepStmt.setInt(1, team.getId());
			prepStmt.setString(1, team.getName());
			prepStmt.executeUpdate();
			bool = true;
			prepStmt.close();
		} catch (SQLException e) {
			if (connector == null) {
				e.printStackTrace();
				throw new DBException(e.getMessage(), e.getCause());
			}
			//e.printStackTrace();
		} finally {
			try {
				if (connector != null) {
					getStmt = connector.prepareStatement(get);
					getStmt.setString(1, team.getName());
					ResultSet result = getStmt.executeQuery();
					if (!result.next()) {
						throw new SQLException();
					}
					team.setId(result.getInt("id"));
				} else {
					throw new SQLException();
				}
			} catch (SQLException e){
				e.printStackTrace();
				throw new DBException(e.getMessage(), e.getCause());
			}
			//throw new DBException(e.getMessage(), e.getCause());
		}
//		try {
//			prepStmt.close();
//		} catch (SQLException e){
//			e.printStackTrace();
//			throw new DBException(e.getMessage(), e.getCause());
//		}
		return bool;
	}

	public boolean deleteUsers(User... users) throws DBException {
		//return false;
		String sql = "DELETE FROM users WHERE login = ?";
		String clearSql = "DELETE FROM users_teams WHERE user_id = (SELECT id FROM users WHERE login = ?)";
		if (connector == null) {
			connect();
		}
		try (PreparedStatement stmt = connector.prepareStatement(sql);
			 PreparedStatement clear = connector.prepareStatement(clearSql)){
			for (User i : users){
				stmt.setString(1, i.getLogin());
				stmt.executeUpdate();
				clear.setString(1, i.getLogin());
				clear.executeUpdate();
			}
			stmt.close();
			clear.close();
			return true;
		} catch (SQLException e){
			e.printStackTrace();
			//throw new DBException(e.getMessage(), e.getCause());
			return false;
		}
	}

	public boolean deleteTeam(Team team) throws DBException {
		//return false;
		String sql = "DELETE FROM teams WHERE name = ?";
		String clearSql = "DELETE FROM users_teams WHERE team_id = (SELECT id FROM teams WHERE name = ?)";
		if (connector == null) {
			connect();
		}
		try	(PreparedStatement stmt = connector.prepareStatement(sql);
				PreparedStatement clear = connector.prepareStatement(clearSql)){
			stmt.setString(1, team.getName());
			stmt.executeUpdate();
			clear.setString(1, team.getName());
			clear.executeUpdate();
			stmt.close();
			clear.close();
			return true;
		} catch (SQLException e){
			e.printStackTrace();
			//throw new DBException(e.getMessage(), e.getCause());
			return false;
		}
	}

	public boolean updateTeam(Team team) throws DBException {
		//return false;
		String sql = "UPDATE teams SET name = ? WHERE id = ?";
		if (connector == null) {
			connect();
		}
		try (PreparedStatement stmt = connector.prepareStatement(sql)){
			stmt.setString(1, team.getName());
			stmt.setInt(2, team.getId());
			stmt.executeUpdate();
			stmt.close();
			return true;
		} catch (SQLException e){
			e.printStackTrace();
			//throw new DBException(e.getMessage(), e.getCause());
			return false;
		}
	}

	public Team getTeam(String name) throws DBException {
		//return null;
		String sql = "SELECT * FROM teams WHERE name = ?";
		if (connector == null) {
			connect();
		}

		try (PreparedStatement stmt = connector.prepareStatement(sql)){
			stmt.setString(1, name);
			ResultSet result = stmt.executeQuery();
			Team team;
			if (result.next()){
				team = new Team(result.getInt("id"), result.getString("name"));
			} else {
				team = null;
			}
			result.close();
			stmt.close();
			return team;
		} catch (SQLException e){
			e.printStackTrace();
			throw new DBException(e.getMessage(), e.getCause());
		}
	}

	public List<User> findAllUsers() throws DBException {
		//return null;
		String sql = "SELECT * FROM users";
		if (connector == null) {
			connect();
		}
		Statement stmt;
		try {
			stmt = connector.createStatement();
			ResultSet result = stmt.executeQuery(sql);
			List<User> list = new LinkedList<>();
//			list.add(new User(result.getInt("id"), result.getString("login")));
//			while (result.next()) {
//				list.add(new User(result.getInt("id"), result.getString("login")));
//			}
			if (result.next()) {
				do {
					list.add(new User(result.getInt("id"), result.getString("login")));
				} while (result.next());
			}
			stmt.close();
			result.close();
			return list;
		} catch (SQLException e){
			e.printStackTrace();
			throw new DBException(e.getMessage(), e.getCause());
		}
	}

	public User getUser(String login) throws DBException {
		//return null;
		String sql = "SELECT * FROM users WHERE login = ?";
		if (connector == null) {
			connect();
		}

		try (PreparedStatement stmt = connector.prepareStatement(sql)){
			stmt.setString(1, login);
			ResultSet result = stmt.executeQuery();
			User user;
			if (result.next()){
				user = new User(result.getInt("id"), result.getString("login"));
			} else {
				user = null;
			}
			result.close();
			stmt.close();
			return user;
		} catch (SQLException e){
			e.printStackTrace();
			throw new DBException(e.getMessage(), e.getCause());
		}
	}

	public List<Team> findAllTeams() throws DBException {
		//return null;
		String sql = "SELECT * FROM teams";
		if (connector == null) {
			connect();
		}
		Statement stmt;
		try {
			stmt = connector.createStatement();
			ResultSet result = stmt.executeQuery(sql);
			List<Team> list = new LinkedList<>();
//			list.add(new Team(result.getInt("id"), result.getString("name")));
//			while (result.next()) {
//				list.add(new Team(result.getInt("id"), result.getString("name")));
//			}
			if (result.next()){
				do {
					list.add(new Team(result.getInt("id"), result.getString("name")));
				} while (result.next());
			}
			stmt.close();
			result.close();
			return list;
		} catch (SQLException e){
			e.printStackTrace();
			throw new DBException(e.getMessage(), e.getCause());
		}
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		//return false;
		String sql = "INSERT INTO users_teams VALUES (?, ?)";
		String sqlUser = String.format("SELECT id FROM users WHERE login = '%s'", user.getLogin());
		String sqlTeam = "SELECT id FROM teams WHERE name = ?";
		if (connector == null) {
			connect();
		}
		try (PreparedStatement insert = connector.prepareStatement(sql); Statement userQuery = connector.createStatement();
			 PreparedStatement teamQuery = connector.prepareStatement(sqlTeam)){
			ResultSet result = userQuery.executeQuery(sqlUser);
			if (!result.next()){
				throw new SQLException();
			}
			int userId = result.getInt("id");
			int teamId;
			connector.setAutoCommit(false);
			for (Team i : teams){
				insert.setInt(1, userId);
				teamQuery.setString(1, i.getName());
				result = teamQuery.executeQuery();
				if (!result.next()){
					throw new SQLException();
				}
				teamId = result.getInt("id");
				insert.setInt(2, teamId);
				insert.executeUpdate();
			}
			connector.commit();
			connector.setAutoCommit(true);
			result.close();
			insert.close();
			teamQuery.close();
			userQuery.close();
			return true;
		} catch (SQLException e){
			//e.printStackTrace();
			if (connector != null){
				try {
					connector.rollback();
					connector.setAutoCommit(true);
					throw new DBException(e.getMessage(), e.getCause());
					//return false;
				} catch (SQLException error){
					error.printStackTrace();
					throw new DBException(error.getMessage(), error.getCause());
				}
			}
			//throw new DBException(e.getMessage(), e.getCause());
			return false;
		}
	}

	public List<Team> getUserTeams(User user) throws DBException {
		//return null;
		String sql = "SELECT * FROM teams WHERE id = ANY(" +
				"SELECT team_id FROM users_teams WHERE user_id = (SELECT id FROM users WHERE login = ?))";
		if (connector == null) {
			connect();
		}
		try (PreparedStatement stmt = connector.prepareStatement(sql)){
			stmt.setString(1, user.getLogin());
			ResultSet result = stmt.executeQuery();
			List<Team> list = new LinkedList<>();
//			list.add(new Team(result.getInt("id"), result.getString("name")));
//			while (result.next()) {
//				list.add(new Team(result.getInt("id"), result.getString("name")));
//			}
			if (result.next()){
				do {
					list.add(new Team(result.getInt("id"), result.getString("name")));
				} while (result.next());
			}
			stmt.close();
			result.close();
			return list;
		} catch (SQLException e){
			e.printStackTrace();
			throw new DBException(e.getMessage(), e.getCause());
		}
	}

}
